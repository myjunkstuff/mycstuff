#include <iostream>
using namespace std;

const int maxels = 5;
int findmax(int [maxels]);

int main()
{
	int nums[maxels] = {2, 18, 1, 27, 16};

cout << "the max value is " << findmax(nums) << endl;

return 0;
}

int findmax(int vals[maxels])
{
	int i, max = vals[0];

	for (i = 1; i < maxels; i++)
		if (max < vals[i])
			max = vals[i];

	return max;
}
