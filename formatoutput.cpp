#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	cout << setw(3) << 6 << endl
		<< setw(3) << 18 << endl
		<< setw(3) << 124 << endl
		<< "---\n"
		<< (6+18+124) << endl;

	cout << "|" << setw(10) << setiosflags(ios::left) << 142 << "|" << endl;
	
	cout << "the decimal (base 10) value of 15 is " << 15 << endl;
	cout << "the octal (base 8) value of 15 is " << showbase << oct << 15 << endl;
	cout << "the hexadecimal (base 16) value of 15 is " << showbase << hex << 15 << endl;
		
	cout << "the decimal value of 025 is " << showbase << dec << 025 << endl;
	cout << "the decimal value o f0x37 is " << showbase << dec << 0x37 << endl;
	return 0;
}
