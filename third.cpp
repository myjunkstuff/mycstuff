#include <iostream>
using namespace std;

int main()
{
	int sum;

	sum = 25;
	cout << "the value of sum is " << sum << endl;
	
	cout << "the value of sum is NOT " << sum + 96 << endl;
	cout << "it is also NOT " << (sum + 96) << endl;

	sum = sum + 69;
	cout << "however the new value is " << sum << endl;
	return 0;
}
