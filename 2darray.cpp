#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	const int numrows = 3;
	const int numcols = 4;

	int i, j;
	int val[numrows][numcols] = {8,16,9,52,3,15,27,6,14,25,2,10};

	cout << "\nDisplay of val array by explicit element"
		<< endl << setw(4) << val[0][0] << setw(4) << val[0][1]
		<< setw(4) << val[0][2] << setw(4) << val[0][3]
		<< endl << setw(4) << val[1][0] << setw(4) << val[1][1]
		<< setw(4) << val[1][2] << setw(4) << val[1][3]
		<< endl << setw(4) << val[2][0] << setw(4) << val[2][1]
		<< setw(4) << val[2][2] << setw(4) << val[2][3];

	cout << "\n\nDisplay of val array using a nest for loop";

	for (i = 0; i < numrows; i++)
	{
		cout << endl;
		for ( j = 0; j < numcols; j++)
			cout << setw(4) << val[i][j];
	}

	cout << endl;

	return 0;
}
