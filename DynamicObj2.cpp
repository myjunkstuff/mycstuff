#include <iostream>
#include <ctime>
#include <cmath>
using namespace std;

/*precondition: srand() must be called once before any memb meth*/
/*postcondit: getitems() returns an int rand no. of items 1-20*/
/*gettime() returns an arrival time 0.0-3.0*/

class Checkout
{
	private:
		/*no class vars*/
	public:
		Checkout() {cout << "\n**** a new cust has arrived****\n";};
		~Checkout()
		{cout << "!!!! this cust obj has been delted!!!!\n";};
		int getitems(){return(1 + rand() % 15);};
		double gettime(){return((double(rand())/Rand_Max)*3);};
};

int main()
{
	Checkout *anothertrans; /*declare a pointer to a checkout obj*/
	int i, howmany;

	cout << "enter the number of simulations to be created: ";
	cin >> howmany;

	srand(time(NULL));
	for(i = 1; i <= howmany; i++)
	{
		/*created new checkout obj*/
		anothertrans = new Checkout;

		/*use pointer to access the member meth's*/
		cout << "the arrival time is " << anothertrans->getTime() << endl;
		cout << "the number of items is "<< anothertrans->getItems() << endl;

		/*delete obj*/
		delete anothertrans;
	}

	return 0;
}
