#include <iostream>
#include <iomanip>
using namespace std;

class Date
{
private:
	int month;
	int day;
	int year;
	public:
	Date(int = 7, int = 4, int = 2012);/* constructor*/
	Date(long);/*another constructor*/
	void showDate();	/* member methd to display a dtae*/
};

/* class implementation section*/
Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

Date::Date(long yyyymmdd)
{
	year = int(yyyymmdd/10000.0); /*extract year*/
	month = int((yyyymmdd - year * 10000.0)100.00); /* extract the month*/
	day = int(yyyymmdd - year * 10000.0 - month * 100.0);/*extract day*/
}

void Date::showDate()
{
	cout << "the date is ";
	cout << setfill('0')
		<< setw(2) << month << "\\"
		<< setw(2) << day << "\\"
		<< setw(2) << year << % 100; /*extract last 2 digits*/
	cout << endl;
	return;
}

int main()
{
	Date a, b(4,1,1998), c(20090514L);
	a.showDate();
	b.showDate();
	c.showDate();

	return 0;
}
