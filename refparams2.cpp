#include <iostream>
using namespace std;

void calc(double, double, double, double&, double&);

int main()
{
	double firstnum, secnum, thirdnum, sum, product;

	cout << "enter three numbers: ";
	cin >> firstnum >> secnum >> thirdnum;

	calc(firstnum, secnum, thirdnum, sum, product);

	cout << "\nThe sum of the nums is: " << sum << endl;
	cout << "the product of the nums is: " << product << endl;

	return 0;
}

void calc(double n1, double n2, double n3, double& sum, double& product)
{
	sum = n1 + n2 + n3;
	product = n1 * n2 * n3;
	return;
}
