#include <iostream>
using namespace std;

int linearsearch(int [], int, int);

int main()
{
	const int numel = 10;
	int nums[numel] = {5,10,22,32,45,67,73,98,99,101};
	int item, location;

	cout << "enter a item you are searching for: ";
	cin >> item;

	location = linearsearch(nums, numel, item);

	if (location > -1)
		cout << "the item was found at index location "
			<< location << endl;
	else
		cout << "the item was not found in the list\n";

	return 0;
}

int linearsearch(int list[], int size, int key)
{
	int i;

	for (i=0; i < size; i++)
	{
		if (list[i] == key)
			return i;
	}
	return -1;
}
