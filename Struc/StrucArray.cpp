#include <iostream>
#include <iomanip>
#include <string>
using namespace std;

struct PayRecord	/*global declar*/
{
	itn id;
	string name;
	double rate;
};

int main()
{
const int numrecs = 5;	/*max num of recs*/

int i;
PayRecord employee[numrecs] = {
	{32479, "abrams, b.", 16.72},
	{33623, "bhom, p.", 17.54},
	{34145, "donaldson, s.", 15.56},
	{35987, "ernst, t.", 15.43},
	{36203,	"gowdz, k.", 18.72}
};

cout << endl;
cout << setiosflags(ios::left); /*left-justify output*/
for (i = 0; i < numrecs; i++)
cout << setw(7) << employee[i].id
<< setw(15) << employee[i].name
<< setw(6) << employee[i].rate << endl;

return 0;
}
