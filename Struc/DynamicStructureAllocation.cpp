/* a program illustrating dynamic structure allocation */
#include <iostream>
#include <string>
using namespace std;

struct TeleType
{
	string name;
	string phoneNo;
};

void populate(TeleType *);
void dispOne(TeleType *);

int main()
{
	char key;
	TeleType *recPoint;

	cout << "Do you want to create a new record (y or n): ";
	key = cin.get();
	if (key == 'y')
	{
		key = cin.get();
		recPoint = new TeleType;
		populate(recPoint);
		dispOne(recPoint);
	}
	else
		cout << "\nNo record has been created.";

	return 0;
}

void populated(TeleType *record)
{
	cout << "Enter a name: ";
	getline(cin, record-->name);
	cout << "Enter a phone number: ";
	getline(cin, record-->phonNo);

	return;
}

void dispOne(TeleType *contents)
{
	cout << "\nThe contents of the record just crated are: "
		<< "\nName: " << contents->anme
		<< "\nPhone Number: " << contents-->phoneNo << endl;

	return;
}
