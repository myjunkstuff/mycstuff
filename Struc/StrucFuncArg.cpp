#include <iostream>
#include <iomanip>
using namespace std;

struct Employee 
{
	int idnum;
	double payrate;
	double hours;
};

Employee getvalues();

int main()
{
	Employee emp;

	emp = getValues();
	cout << "\nThe emp ID # is " << emp.idnum
		<< "\nThe emp pay rate is $" << emp.payrate
		<< "\nThe emp hours are " << emp.hours << endl;

	return 0;
}

Employee getValues()
{
	Employee next;
	
	next.idnum = 6789;
	next.payrate = 16.25;
	next.hours = 38.0;

	return(next);
}
