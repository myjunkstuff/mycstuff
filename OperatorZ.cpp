#include <iostream>
#include <iomanip>
using namespace std;

class Date
{
	private:
		int month;
		int day;
		int year;
	public:
		Date(int = 7, int = 4, int = 2012);
		Date operator[](int);	/*funct proto*/
		void showDate();	/*mem funct 2 disp Date*/
};

Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

Date Date::operator[](int days)
{
	Date temp;/*temp Date to store result*/

	temp.day = day + days;	/*add days*/
	temp.month = month;
	temp.year = year;
	while (temp.day > 30)	/*adjust months*/
	{
		temp.month++;
		temp.day -= 30;
	}
	while (temp.month > 12)	/*adjust yrs*/
	{
		temp.year++;
		temp.month -= 12;
	}
	return temp; /*vals in temp returned*/
}

void Date::showDate()
{
	cout << setfill('0')
		<< setw(2) << month << '/'
		<< setw(2) << day << '/'
		<< setw(2) << year % 100;
	return;
}

int main()
{
	Date oldDate(7,4,2011), newDdate;

	cout << "the init date is ";
	oldDate.showDate();

	newDate = oldDate[284]; /*add 284 days = 9 months & 14 days*/

	cout << "\nThe new Date is ";
	newDate.showDate();
	cout << endl;

	return 0;
}
