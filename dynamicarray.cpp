#include <iostream>
#include <new>
using namespace std;

int main()
{
	int numgrades, i;

	cout << "enter the number of grades to be processed: ";
	cin >> numgrades;

	int *grades = new int(numgrades); // create array

	for(i=0; i < numgrades; i++)
	{
		cout << " enter a grade: ";
		cin >> grades[i];
	}
	cout << "\nAn array was created for " << numgrades << " integers\n";
	cout << " The values stored in the array are:";
	for (i =0; i < numgrades; i++)
		cout << "\n " << grades[i];
	cout << endl;

	delete[] grades;  // return the storage to the heap
	
	return 0;
}
