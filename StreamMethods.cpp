#include <iostream>
#include <fstream>
#include <cstdlib>	/*needed for exit()*/
using namespace std;

int main()
{
	ifstream inFile;
	ofstream outFile;
	char response;
	inFile.open("prices.dat");	/*attempt to open file for input*/

	if (!inFile.fail())	/* if it doesnt fail, the file exists*/
	{
		cout << "a file by the name prices.dat exists.\n"
			<< "do you want to continue and overwrite it\n"
			<< " with the new data (y or n): ";
		cin >> response;
		if (tolower(response) == 'n')
		{
			cout << "the existing file will not be overwritten." << endl;
			exit(1); /* terminate program execution */
		}
	}

	outFile.open("prices.dat"); /* now open the file for writing*/

	if (inFile.fail())	/* check for successful open*/
	{
		cout << "\nThe file was not opened" << endl;
		exit(1);
	}

	cout << "the file has been succesfully opened for output." << endl;

	/* statements to write the file would be placed here */

	return 0;
}
