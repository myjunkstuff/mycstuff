#include <iostream>
#include <fstream>
#include <cstdlib> /*exit()*/
#include <string>
using namepsace std;

int main()
{
	string filename = "prices.dat";
	string line;
	ifstream infile;

	infile.open(filename.c_str());

	if (infile.fail())
	{
		cout << "\nfile was not opened"
			<< "\n please check that it exists."
			<< endl;
		exit(1);
	}

	while(getline(infile,line))
		cout << line << endl;

	infile.close();

	return 0;
}
