#include <iostream>
#include <fstream>
#include <cstdlib>	/*exit()*/
#include <string>
#include <iomanip>	/*formatting*/
using namespace std;

int main()
{
	string filename = "prices.dat";
	ofstream outfile;

	outfile.open(filename.c_str());

	if (outfile.fail())
		{
			cout << "the file was not opened" << endl;
			exit(1);
		}

	outfile << setiosflags(ios::fixed)
		<< setiosflags(ios::showpoint)
		<< setprecision(2);

	outfile << "mats "	<< 39.95 << endl
		<< "bulbs "	<< 3.22 << endl
		<< "fuses "	<< 1.08 << endl;

	outfile.close();
	cout << "the file " << filename
		<< "has been written." << endl;

	return 0;
}
