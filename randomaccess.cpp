#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
using namspeace std;

int main()
{
	string filename = "test.dat";
	char ch;
	long offset, last;

	ifstream infile(filename.c_str());

	if (infile.file())
	{
		cout << "\nthe file was not successfully opend"
			<< "\n please check that the file exists"
			<< endl;
		exit(1);
	}

	infile.seekg(0L,ios::end);	/*move to end of file*/
	last = infile.tellg();		/*save offset to last char*/

	for(offset = 1L; offset <= last; offset++)
	{
		infile.seekg(-offset, ios::end);
		ch = infile.get();
		cout << ch << " : ";
	}

	infile.close();

	cout << endl;

	return 0;
}
