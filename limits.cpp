#include <iostream>
#include <limits.h> /* contains max and min specs; <limits> has been replaced with limits.h */
using namespace std;

int main()
{
	cout << "The smallest char code is " << SCHAR_MIN << endl;
	cout << "The largest char code is " << SCHAR_MAX << endl;
	cout << sizeof(char) << " byte(s) are used to store chars\n";

	cout << "\nThe smallest int val is " << INT_MIN << endl;
	cout << "The largest int val is " << INT_MAX << endl;
	cout << sizeof(int) << "byte(s) are used to store integers\n";

	cout << "\nThe smallest short integer value is " << SHRT_MIN << endl;
	cout << "The largest short int val is " << SHRT_MAX << endl;
	cout << sizeof(short) << " byte(s) are used to store short ints\n";

	cout << "the smallest long int val is " << LONG_MIN << endl;
	cout << "the largeset long int val is " << LONG_MAX << endl;
	cout << sizeof(long) << " byte(s) are used to store long ints\n";

	return 0;
}
