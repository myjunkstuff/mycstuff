#include <iostream>
using namespace std;

class Checkout
{
	private:
		int numitems;
		double arrivaltime;

	public:
		Checkout();/*constructor*/
		/*inline destructor vvv*/
		~Checkout()
		{cout << "!!!! this customer obj has been deleted!!!!\n";}'
		void showobject();
		void getitems(){return;};/*inline do-nothing meth*/
		void gettime() {return;};/*used in DynamicObj2.cpp*/
};

Checkout::Checkout()
{
	cout << "\n**** A new customer obj has been created ****\n";
	numitems = 5;
	arrivaltime = 2.5;
}

void Checkout::showObject()
{
	cout << "	For this obj:\n";
	cout << "	numitems = " << numitems
		<< "	arrivaltime = " << arrivaltime << endl;
	return;
}

int main()
{
	Checkout *anothertrans;/*pointer to checkout obj*/
	int i, howmany;

	cout << "enter the number of transactions to be created: ";
	cin >> howmany;

	for(i = 1; i <= howmany; i++)
	{
		anothertrans = new Checkout; /*new checkout obj*/

		/*display addr of created obj*/
		cout << "the memory address of this object is: " << anothertrans << endl;
		anothertrans->showobject();/*display contents of this obj*/
		delete anothertrans;/*delted obj*/
	}

	return 0;
}
