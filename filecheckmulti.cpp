#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;

int main()
{
	string fileone = "info.txt";
	string filetwo = "info.bak";
	char ch;
	ifstream inFile;
	ofstream outFile;

	try
	{
		/* basic input */
		inFile.open(fileOne.c_str());
		if (inFile.fail()) throw fileOne;
	}
	catch (string in) /* catch for outer try block */
	{
		cout << "the input file " <<  in
			<< " was not opened." << endl
			<< " no backup was made." << endl;
		exit(1);
	}

	try /* this block tries to open the output files and perform all process*/
	{
		outFile.open(fileTwo.c_str());

		if (outFile.fail())throw fileTwo;

		while ((ch = inFile.get())!= EOF)
			outFile.put(ch);
		inFile.close();
		outFile.close();
	}
	catch (string out)	/* catch for inner try block */
	{
		cout << "the backup file " << out
			<< "was not succcessfully opened. " << endl;
		exit(1);
	}

	cout << "a back up of " << fileOne
		<< "named " << fileTwo << " was made." << endl;

	return 0;
}
