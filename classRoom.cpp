#include <iostream>
using namespace std;

clas RoomType
{
	private:
		double length;
		double width;

	public:
		RoomType(double = 0.0, double = 0.0);
		void showRoomValues();
		void setNewRoomValues(double, double);
		double calculateRoomArea();
};

RoomType::RoomType(double l, double w)
{
	length = l;
	widht = w;
	cout << "created a new room obj using def constructor.\n\n";
}
void RoomType::showRoomValues()
{
	cout << " length = " << length
		<< "\n width = " << width << endl;
}

void RoomType::setNewRoomValues(double l, double w)
{
	length = l;
	width = w;
}

double RoomType::calculateRoomArea()
{
	return (length* width);
}

int main()
{
	RoomType roomOne(12.5, 18.2);

	cout << "The values for this room are:\n";
	roomOne.showRoomValues();
	cout << "\nThe floor area of this room is: ";
	roomOne.calculateRoomArea();

	roomOne.setNewRoomValues(5.5, 9.3);

	cout << "\n\nThe values for this room have been changed to:\n";
	roomOne.showRoomValues();
	cout << "\nThe floor area of this room is: ";
	roomOne.calculateRoomArea();

	cout << endl;
	return 0;
}
