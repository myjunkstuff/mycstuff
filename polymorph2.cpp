#include <iostream>
#include <cmath>
using namespace std;

class One
{
	protected:
		double a;
	public:
		One(double = 2.0;/*constructor*/
				{virtual double f1(double);/*memb funct*/
				double f2(double);
				};

				One::One(double val)/*constructor*/
				{
				a= val;
				}

				double One::f1(double num)/*memb funct*/
				{
				return(num/2);
				}

double One::f2(double num)/*another memb funct*/
{
return( pow(f1(num),2) );/*square result of f1()*/
}

class Two : public One
{
	public:
		virtual double f1(double);/*overrides class One's f1()*/
};

double Two::f1(double num)
{
	return(num/3);
}

int main()
{
	One object_1;/*obj 1 is an obj of base class*/
	Two object_2;/* obj 2 is obj of derived class*/

	/*call f2() using bas class obj call*/
	cout << "The computed value  using a base class obj call is "
		<< object_1.f2(12) << endl;

	/*call f2() using derived*/
	cout << "the computed value using a derived class obj call is "
		<< object_2.f2(12) << endl;

	return 0;
}
