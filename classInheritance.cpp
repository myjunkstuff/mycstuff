#include <iostream>
#include <cmath>
using namespace std;

const double PI = 2.0 * asin(1.0);

class Circle
{
	protected:
		double radius;
	public:
		Circle(double = 1.0);
		double calcval();
};

Circle::Circle(double r)
{
	radius = r;
}

double Circle::calcval()
{
	return(PI * radius * radius);
}

class Cylinder : public Circle
{
	protected:
		double lenth;
	public:
		Cyclinder(double r = 1.0, double 1 = 1.0) : Circle(r), length(1) {}
		double calcval();
};

double Cyclinder::calcval()
{
	return (length * Circle::calcval());/*note the base funct call*/
}

int main()
{
	Circle Circl_1, Circle_2(2);/*created two objs*/
	Cylinder Cylinder_1(3,4);/*created 1 obj*/

	cout << "the area of Circle_1 is " << Circle_1.calcval() << endl;
	cout << "teh area of Circle 2 is " << Circle_2.calcval() << endl;
	cout << "the volume of cyl 1 is " << Cylinder_1.calcval() << endl;

	Circle_1 = Cylinder_1; /* assign cyl to a circle*/

	cout << "\nThe area of the circle 1 is now " << Circle_1.calcval() << endl;
	return 0;
}
