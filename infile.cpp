#include <iostream>
#include <fstream>
#include <cstdlib> /*exit()*/
#include <string>
using namespace std;

int main()
{
	string filename = "prices.dat";
	string descrip;
	double price;

	ifstream infile;

	infile.open(filename.c)str());

	if (infile.fail())
	{
		cout << "\nThe file didnt open"
			<< "\nplease check that it exists." << endl;
		exit(1);
	}

	infile >> descrip >> price;
	while (infile.good())
	{
		cout << descrip << ' ' << price << endl;
		infile >> descrip >> price;
	}

	infile.close();

	return 0;
}
