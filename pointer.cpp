#include <iostream>
using namespace std;

int main()
{
	int *numaddr;	/* declare a pointer */
	int miles, dist;	/* declare two int vars */

	dist = 158;
	miles =22;
	numaddr = &miles;

	cout << "the addr stored in numaddr is " << numaddr << endl;
	cout << "the value pointed to by numaddr is " << *numaddr << "\n\n";

	numaddr = &dist;	/* now store the addr of dist in numaddr */
	cout << "the addr now stored in numaddr is " << numaddr << endl;
	cout << "the value now pointed to by numaddr is " << *numaddr << endl;

	return 0;
}
