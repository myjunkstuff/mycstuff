#include <iostream>
using namespace std;
void findmax(int, int); // the function declaration

int main()
{
	int firstnum, secnum;

	cout << "\nEnter a number: ";
	cin >> firstnum;
	cout << "great! please enter a second number: ";
	cin >> secnum;

	findmax(firstnum, secnum); // the function is called here
	
	return 0;
}

// following is the function findmax()

void findmax(int x, int y)
{
	int maxnum;

	if (x >= y)
		maxnum = x;
	else
		maxnum = y;

	cout << "\nThe maxnum of the two nums is "
		<< maxnum << endl;

	return;
}
