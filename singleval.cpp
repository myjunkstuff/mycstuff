#include <iostream>
using namespace std;

int findmax(int, int);

int main()
{
	int firstnum, secnum, max;
	cout << "\nEnter a number: ";
	cin >> firstnum;
	cout << "great! enter a second number: ";
	cin >> secnum;

	max = findmax(firstnum, secnum);

	cout << "\nThe max of two nums is " << max << endl;

	return 0;
}

int findmax(int x, int y)
{
	int maxnum;

	if (x >= y)
		maxnum = x;
	else
		maxnum = y;

	return maxnum;
}
