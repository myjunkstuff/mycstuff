#include <iostream>
using namespace std;

int main()
{
	cout << "\nData Type	Bytes";
	cout << "\n---------	-----";
	cout << "\nint		" << sizeof(int);
	cout << "\nchar		" << sizeof(char);
	cout << "\nbool		" << sizeof(bool);
	cout << "\n";

	cout << "the sum of 6 and 15 is " << (6 + 15) << "\n";

	int number = 13;
	double dble = 13.3;
	cout << "variable number is " << number << "\n";
	cout << "double dble is " << dble << "\n";
	int numA = 1;
	int numB = 2;
	cout << " num-a + num-b is " << numA + numB;
	cout << "\n";
	return 0;
}
