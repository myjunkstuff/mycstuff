#include <iostream>
#include <fstream>
#include <cstdlib> /* need for exit() */
#include <string>
#include <iomanip> /* formatting */
using namespace std;

int main()
{
	char response;
	string filename = "prices.dat";
	ifstream inFile;
	ofstream outFile;

	try /* open a basic input stream to check file exists */
	{
		inFile.open(filename.c_str());

		if (inFile.fail()) throw 1; /* thos means the file not exist */
		/* only get here if file found, else catch block takes cntrl */
		cout << "a file by the anme " << filename << " currently exists. \n"
			<< "Do you want to overwrite it with the new data (y or n): ";
		cin >> response;

		if (tolower(response) == 'n')
		{
			inFile.close();
			cout << "The existing file has not been overwritten." << endl;
			exit(1);
		}
	}
	catch(int e) {}; /* do-nothing block that permits processing to cont. */
		try
		{
			/* open the file in write mode to cont with file writes */
			outFile.open(filename.c_str());

			if (outFile.fail()) throw filename;
			/* set the output file stream formats */
			outFile << setiosflags(ios::fixed)
				<< setiosflags(ios::showpoint)
				<< setprecision(2);
			/* write data to file */
			outFile << "mats " << 39.95 << endl
				<< "bulbs " << 3.22 << endl
				<< "fuses" << 1.08 << endl;
			outFile.close();
			cout << "the file " << filename
				<< " has been written." << endl;
			return 0;
		}
		catch(string e)
		{
			cout << "the file " << filename
				<< " was not opened for output and has not been written."
				<< endl;
		}
	}
