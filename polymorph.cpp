#include <iostream>
#include <cmath>
using namespace std;

class One
{
	protected:
		double a;
	public:
		One(double = 2.0);
		double f1(double);
		double f2(double);
};

One::One(double val)
{
	a = val;
}

double One::f1(double num)
{
	return(num/2);
}

double One::f2(double num)
{
	return( pow(f1(num),2) );
}

class Two : public One
{
	public:
		double f1(double);/*this overrides class One's f1()*/
};

double Two::f1(double num)
{
	return(num/3);
}

int main()
{
	One object_1;/*obj 1 is an obj of the base class*/
	Two object_2;/*obj 2 is an obj of the derived class*/

	/*call f2() using a base class obj call*/
	cout << "the computed value using a base class obj call is "
		<< object_1.f2(12) << endl;

	/*call f2() using a derived class obj call*/
	cout << "the copmuted value using a derived class obj call is "
		<< object_2.f2(12) << endl;

	return 0;
}
