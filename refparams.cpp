#include <iostream>
using namespace std;

void newval(double&, double&);

int main()
{
	double firstnum, secnum;

	cout << "enter two numbers: ";
	cin >> firstnum >> secnum;
	cout << "\nthe val of firstnum is: " << firstnum << endl;
	cout << "the val is secnum is: " << secnum << "\n\n";

	newval(firstnum, secnum);

	cout << "the val in firstnum is now: " << firstnum << endl;
	cout << "the val of secnum is now: " << secnum << endl;

	return 0;
}

void newval(double& xnum, double& ynum)
{
	cout << "the value is xnum is: " << xnum << endl;
	cout << "the value in ynum is: " << ynum << "\n\n";
	xnum = 89.5;
	ynum = 99.5;

	return;
}
