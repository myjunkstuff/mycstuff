#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	/* remember constants don't change after */
	const double SALESTAX = 0.05;
	double amount, taxes, total;

	cout << "the salestax rate is " << SALESTAX << endl;
	cout << "\nEnter the amount purchased: ";
	cin >> amount;
	taxes = SALESTAX * amount;
	total = amount + taxes;
	cout << setiosflags(ios::fixed)
		<< setiosflags(ios::showpoint)
		<< setprecision(2);
	cout << "the sales tax is " << setw(4) << taxes << endl;
	cout << "the total bills is " << setw(5) << total << endl;
	
	return 0;
}
