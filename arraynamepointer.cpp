#include <iostream>
using namespace std;

int main()
{
	const int arraysize = 5;

	int i, grade[arraysize] = {98,87,92,79,85};

	for (i=0; i < arraysize; i++)
		cout << "\nElement " << i << " is " << grade[i];

	cout << endl;

	return 0;
}
