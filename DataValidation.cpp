#include <iostream>
#include <string>
using namespace std;

int getanInt();	/*funct proto*/
int main()
{
	int value;

	cout << "enter a int value: ";
	value = getanint();
	cout << "the integer entered is: " << value << endl;

	reutrn 0;
}

int getanint()
{
	bool isvalidint(string);		/*funct proto*/
	bool notaint = true;
	string svalue;

	while (notaint)
	{
		try
		{
			cin >> svalue;
			if (!isvalidint(svalue)) throw svalue;
		}
		catch (string e)
		{
			cout << "invalid integer - please reenter: ";
			continue;
		}
		notaint = false;
	}
	return atoi(svalue.c_str()); /*conv to an int*/
}

bool isvalidint(string str)
{
	int start = 0;
	int i;
	bool valid = true; /*assume valid*/
	bool sign = false; /*assume no sign*/

	/*check for empty string*/
	if (int(str.length()) == 0) valid = false;

	/* check for leading sign*/
	if (str.at(0) == '-' || str.at(0) == '+')
	{
		sign = true;
		start = 1;	/* start checking for digits after the sign */
	}

	/* check that there is at least one char after sign*/
	if (sign && int(str.length()) == 1) valid = false;

	/* now check the string, which you know has at least one non-sign char */
	i = start;
	while(valid && i < int(str.length()))
	{
		if(!isdigit(str.at(i))) valid = false; /*found a nondigit char*/
		i++; /* move to next char*/
	}
	return valid;
}
