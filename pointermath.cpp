#include <iostream>
using namespace std;

int main()
{
	const int values = 5;

	int nums[values] = {16,54,7,43,-5};
	int total = 0, *nPt;

	nPt = nums;	// store address of nums[0] in nPt
	while (nPt < nums + values)
		total += *nPt++;

	cout << "the total of the array elements is " << total << endl;

	return 0;
}
