#include <iostream>
using namespace std;

int main()
{
	int numerator, denominator;
	bool needDenominator = true;

	cout << "enter a numer (whole num only): ";
	cin >> numerator
		cout << "enter a denom (whole only): ";

	while(needDenominator)
	{
		cin >> denominator;
		try
		{
			if (denominator == 0)
				throw denominator;
		}
		catch(int e)
		{
			cout << "a denom val of " << e << "is invalid." << endl;
			cout << "please enter a denom (whole only): ";
			continue; /*send cntrl back to while statement*/
		}
		cout << numerator << '/' << denominator
			<< " = " << double(numerator)/ double(denominator) << endl;
		needDenominator = false;
	}

	return 0;
}
