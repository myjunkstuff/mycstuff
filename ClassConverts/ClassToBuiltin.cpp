#include <iostream>
#include <iomanip>
using namespace std;

class Date
{
	private:
		int month, day, year;
	public:
		Date(int = 7, int = 4, int = 2012);
		operator long();
		void showDate();
};

Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

Date::operator long()/*must return long as name imples*/
	{
	long yyyymmdd;
	yyyymmdd = year * 10000 + month * 100 + day;
	return(yyyymmdd);
	}

void Date::showDate()
{
	cout << setfill('0')
		<< setw(2) << month << '/'
		<< setw(2) << day << '/'
		<< setw(2) << year % 100;
	return;
}

int main()
{
	Date a(4,1,2012);
	long b;

	b = long(a);

	cout << "a's date is ";
	a.showDate();
	cout << "\nThis date, as a long integer, is " << b << endl;

	return 0;
}
