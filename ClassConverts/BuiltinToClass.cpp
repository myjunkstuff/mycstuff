#include <iostream>
#include <iomanip>
using namespace std;

class Date
{
	private:
		int month, day, year;
	public:
		Date(int = 7, int = 4, int = 2012);

		Date(long);
		void showDate();
};

Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

Date::Date(long findate)
{
	year = int(findate/10000.0);
	month = int((findate - year * 10000.0)/100.0);
	day = int(findate - year * 10000.0 - month * 100.0);
}

void Date::showDate()
{
	cout << setfill('0')
		<< setw(2) << month << '/'
		<< setw(2) << day << '/'
		<< setw(2) << year % 100;
	return;
}

int main()
{
	Date a;
	Date b(20061225L);
	Date c(4,1,2007);

	cout << "Dates a, b, and c are ";
	a.showDate();
	cout << ", ";
	b.showDate();
	cout << ", and ";
	c.showDate();
	cout << ".\n";

	a = date(20150103L);

	cout << "Date a is now ";
	a.showDate();
	cout << ".\n";

	return 0;
}
