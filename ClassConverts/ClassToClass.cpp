#include <iostream>
#include <iomanip>
using namespace std;

class Intdate;

class Date
{
	private:
		int month, day, year;
	public:
		Date(int = 7, int = 4, int = 2012);
		operator Intdate();
		void showDate();
};

class Intdate
{
	private:
		long yyyymmdd;
	public:
		Intdate(long = 0);
		operator Date();
		void showint();
};

Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

Date::operator Intdate()
{
	long temp;
	temp = year * 10000 + month * 100 + day;
	return(Intdate(temp));
}

void Date::showDate()
{
	cout << setfill('0')
		<< setw(2) << month << '/'
		<< setw(2) << day << '/'
		<< setw(2) << year % 100;
	return;
}

Intdate::Intdate(long ymd)
{
	yyyymmdd =ymd;
}

Intdate::operator Date()
{
	int mo, da, yr;
	yr = int(yyyymmdd/10000.0);
	mo = int((yyyymmmdd - yr * 10000.0)/100.0);
	da = int(yyyymmdd - yr * 1000.0 - mo * 100.0);
	return(Date(mo,da,yr));
}

void Intdate::showint()
{
	cout << yyyymmdd;
	return;
}

int main()
{
	Date a(4,1,2011), b;/*declare two date objs*/
	Intdate c(20121215L), d;/*declare two intdate objs*/
	b = Date(c);		/* cast c into a date obj*/
	d = Intdate(a);		/*cast a into Intdate obj*/

	cout << " a's date is ";
	a.showDate();
	cout << "\n as an Intdate object this date is ";
	d.showint();

	cout << "\n c's date is ";
	c.showint();
	cout << "\n as a Ddate obj this date is ";
	b.showDate();
	cout << endl;

	return 0;
}
