#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;

int main()
{
	string fileone = "info.txt";
	string filetwo = "info.bak";
	char ch;
	ifstream infile;
	ofstream outfile;

	try
	{
		/*basic input stream*/
		inFile.open(fileone.c_str());
		if (inFile.fail()) throw fileone;
	}
	catch << "the input file " << in
		<< " was not successfully opened." << endl
		<< " no backup was made." << endl;
	exit(1);
}

try 
{
	outFile.open(filetwo.c_str());

	if (outFile.fail())throw fileTwo;

	while ((ch = inFile.get())!= EOF)
		outFile.put(ch);

	inFile.close();
	outFile.close();
}
catch (string out)
{
	cout << "the backup file " << out
		<< " was not succesfully opened." << endl;
	exit(1);
}

cout << "a successful backup of " << fileone
<< " named " << filetwo << " was made." << endl;

return 0;
}
