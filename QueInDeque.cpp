#include <iostream>
#include <deque>
#include <string>
#include <cctyp>
using namespace std;

int main()
{
	string name;
	deque<string> queue;

	cout << "enter as many names as you want, one per line" << endl;
	cout << "to stop enter a single x" << endl;

	/* push names onto the queue*/
	while(true)
	{
		cout << "enter a name (or x to stop): ";
		getline(cin, name);
		if (tolower(name.at(0)) == 'x') break;
		queue.push_front(name);
	}

	cout << "\nthe names in teh queue are:\n";

	/*pop names from the queue*/
	while(!queue.empty())
	{
		name = queue.back();/*get name*/
		queue.pop_back();/*pop name*/
		cout << name << endl;
	}
	return 0;
}
