#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namespace std;

int main()
{
	string fileOne = "info.txt";
	string fileTwo = "info.bak";
	char ch;
	ifstream inFile;
	ofstream outFile;

	try
	{
		inFile.open(fileOne.c_str());
		try
		{
			outFile.open(fileTwo.c_str());
			while ((ch = inFile.get()) != EOF)
				outFile.put(ch);

			inFile.close();
			outFile.close();
		}
		catch (string out)
		{
			cout << "the backkup file " << out
				<< "was not opened." << endl;
			exit(1);
		}
	}
	catch (string in)
	{
		cout << "the input file " << in
			<< " was not opened." << endl
			<< "no backup was made." << endl;
		exit(1);

	}

	cout << "a successful backup of " << fileOne
		<< " named " << fileTwo << "was made." << endl;

	return 0;
}
