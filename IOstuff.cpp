#include <iostream>
using namespace std;

int main()
{
	double num1, num2, num3, average, product;

	num1 = 30.0;
	num2 = 0.05;
	product = num1 * num2;
	cout << "30.0 times 0.05 is " << product << endl;

	cout << "please type in a number: ";
	cin >> num1;
	cout << "please type in another number: ";
	cin >> num2;
	product = num1 * num2;
	cout << num1 << " times " << num2 << " is " << product << endl;

	cout << "enter three numbers: " << endl;
	cin >> num1 >> num2 >> num3;
	average = (num1 + num2 + num3) / 3.0;
	cout << "the average of the numbers is " << average << endl;

	return 0;
}
