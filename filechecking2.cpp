#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <iomanip>
using namespace std;

int main()
{
	char response;
	string filename = "prices.dat";
	ifstream inFile;
	ofstream outFile;

	try /* open basic input stream to check whether the file exists*/
	{
		inFile.open(filename.c_str());

		if (inFile.fail()) throw 1; /*this means file doesnt exist*/
		cout << "a file by the name of " << filename << " currently exists.\n"
			<< do you want to overwrite it with the new data (y or n): ";
				cin >> response;

				if (tolower(response) == 'n')
				{
					inFile.close();
					cout << "the existing file has not been overwritten." << endl;
					exit(1);
				}
	}
	catch(int e) {}; /* donothing block permits process to cont.*/

	try
	{
		/* open file idn write mode and continue with file writes*/
		outFile.open(filename.c_str());

		if (outFile.fail()) throw filename;
		outFile << "mats " << 39.95 << endl
			<< "bulbs " << 3.22 << endl
			<< "fuses " << 1.08 << endl;
		outFile.close();
		cout << "the file " << filename
			<< " has been successfully written." << endl;
		return 0;
	}
	catch(string e)
	{
		cout << "the file " << filename
			<< " was not opened for the output and has not been written."
			<< endl;
	}
}
