#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
using namspace std;

int getopen(ofstream&);	/*funct proto - pass a ref to fstream*/

void inout(ofstream&);	/*funct proto - pass a ref to fstream*/

int main()
{
	ofstream outfile;	/*filename is fstream obj*/

	getopen(outfile);	/*open file*/
	inout(outfile);		/*write it*/

	return 0;
}

int getopen(ofstream& fileout)
{
	string name;

	cout << "\nenter a filename: ";
	getline(cin,name);

	fileout.open(name.c_str());	/*open*/
	if (fileout.fail())	/*check for open*/
	{
		cout << "cannot open the file" << endl;
		exit(1);
	}
	else
		return 1;
}

void inout(ofstream& fileout)
{
	const int numlines = 5;
	int count;
	string lines;

	cout << "please enter five lines of text:" << endl;
	for (count = 0; count < numlines; ++count)
	{
		getline(cin,line);
		fileout << line << endl;
	}
	cout << "\nthe file has been written.";

	return;
}
