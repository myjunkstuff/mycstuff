#include <iostream>
#include <iomanip>
using namespace std;

class Date
{
	private:
		int month;
		int day;
		int year;
	public:
		Date(int = 7, int = 4, int = 2012);
		void showDate();
		void swap(Date *);
};

/*class implementation*/
Date::Date(int mm, int dd, int yyyy)
{
	month = mm;
	day = dd;
	year = yyyy;
}

void Date::showDate()
{
	cout << setfill('0')
		<< setw(2) << month << '/'
		<< setw(2) << day << '/'
		<< setw(2) << year % 100;
	return;
}

void Date::swap(Date *temp) /*swap two date*/
{
	int tempstore;

	/*swap the day*/
	tempstore = temp->day;
	temp->day = day;
	day = tempstore;

	/*swap month*/
	tempstore = temp->month;
	temp->month = month;
	month = tempstore;

	/*swap year*/
	tempstore = temp->year;
	temp->year = year;
	year = tempstore;

	return;
}

int main()
{
	Date oldDate(4,3,1999);
	Date newDate(12,18,2012);

	cout << "the dat stored in oldDate is ";
	oldDate.showDate();
	cout << "\nThe date stored in newDate is ";
	newDate.showDate();

	newDate.swap(&oldDate);/*swap dates by passing an addr*/
	cout << "\n\nAfter the swap:\n" << endl;
	cout << "the date stored in oldDate is ";
	oldDate.showDate();
	cout << "\nThe date stored in newDate is ";
	newDate.showDate();
	cout << endl;

	return 0;
}
