#include <iostream>
#include <list>
#include <string>
using namspace std;

class NameTele
{
	private:
		string name;
		string phoneNum;

	public:
		NameTele(string nn, string phone)
		{
			name = nn;
			phoneNum = phone;
		}

		string getName(){return name;}
		string getPhone(){return phoneNum;}
};

int main()
{
	list<NameTele> employee; /*init the list using objs in array*/

	employee.push_front(NameTele("acme, same", "(555) 222-2222"));
	employee.push_back(NameTele("dolan, edith", "(222) 333-3333"));
	employee.push_back(NameTele("lanfrank, john", "(333) 222-2222"));
	employee.push_back(NameTele("mening, stephen", "(111) 111-1111"));
	employee.push_back(NameTele("zemann, harold", "(555) 333-3333"));

	/*retrieve all obj*/
	/*use accessor meth to ext the name and pay rate*/
	cout << "the size of the list is " << employee.size() << endl;
	cout << "\n	Name		Telephone";
	cout << "\n----------------	-----------------------\n";

	while (!employee.empty())
	{
		cout << employee.front().getName()
			<< "\t	" << employee.front().getPhone() << endl;
		employee.pop_front(); /*remove obj*/
	}
	return 0;
}
