#include <iostream>
#include <list>
#include <algorithm>
#include <string>
using namespace std;

int main()
{
	list<string> names, addnames;
	string n;

	/*add names to list*/
	names.push_front("dolan, edith");
	names.push_back("lanfrank, john");

	/*create new list*/
	addnames.push_front("acme, same");
	addnames.push_front("mening, stephen");
	addnames.push_front("zemann, frank");

	names.sort();
	addnames.sort();

	/*merge second list*/
	names.merge(addnames);
	cout << "the final list size is: " << names.size() << endl;
	cout << "this list contians the names:\n";

	while(!names.empty())
	{
		cout << names.front() << endl;
		names.pop_front();	/*remove obj*/
	}
	return 0;
}
