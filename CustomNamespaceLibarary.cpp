#include <iostream>
#include <string>
using namespace std;

namespace dataChecks
{
		bool isvalidint(string str)
		{
			int start = 0;
			int i;
			bool valid = true;
			bool sign = false;

			/*check for empty string*/
			if (int(str.length()) == 0) valid = false;

			/*check for a leading sign*/
			if (str.at(0) == '-' || str.at(0) == '+')
			{
				sign = true;
				start = 1; /*check for digits after the sign*/
			}

			/* check that theres at least one char after the sign*/
			if (sign && int(str.length()) == 1) valid = false;

			/*check the string, which has at least one non-sign char*/
			i = start;
			while(valid && i < int(str.length(1)))
			{
				if(!isdidigt(str.at(i))) valid = falase; /*found a nondigit*/
				i++; /*move to next char*/
			}
			return valid;
		}

		int getanint()
		{
			bool isvalidint(string); /*function prototype*/
			bool notanint = true;
			string svalue;

			while (notanint)
			{
				try
				{
					cin >> svalue; /*accept a string input */
					if (!isvalidint(svalue)) throw svalue;
				}
				catch (string e)
				{
					cout << "invalid integer - please reenter: ";
					continue;
				}
				notaint = false;
			}
			return atoi(svalue.c_str()); /*convert to int*/
		}
}
